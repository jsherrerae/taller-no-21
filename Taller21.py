from graphics import *

def dibujar(Vo, Yo):
  win = GraphWin('Gráfico', 500, 500)

  win.setCoords(0, 0, 500, 500)
  
  g = 9.8
  t = 0
  Xo = 0
  
  while Yo - (1/2 * g* (t** 2)) >= 0:
    c = Circle(Point(
      Vo * t,
      Yo - (1/2 * g* (t** 2))
    ), 0.5)
    c.setFill('blue')
    c.draw(win)
    
    Xo += 1
    t = Xo / Vo
  
  win.getMouse()
  win.close()

dibujar(100,400)